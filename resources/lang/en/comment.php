<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'title' 		=> 'CommentBox',
    'enter_name' 	=> 'Enter Name',
    'enter_email' 	=> 'Enter Email',
    'write_here_to_send' => 'Write here to send...',
    'submit' 		=> 'Submit',
    'error' 		=> 'Error!!',
    'comment_success' => 'Comment successfully!!',
    'comment_error' => 'Failed to add Comment',
    'comment_reply' => 'Commment Reply',

];
