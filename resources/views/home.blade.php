<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{ trans('comment.title') }}</title>
    <meta name="viewport"content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="">
    <meta name="msapplication-tap-highlight" content="no">
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- jQuery -->
	<script src="{{asset('assets/scripts/jquery-3.5.0.min.js')}}"></script>
	<!-- SWEET ALERT2 -->
	<script src="assets/sweetalert/sweetalert2.js"></script>
	
    <!-- Custom -->
	<link rel="stylesheet" href="assets/jQalertbox/jQalertbox.css">
	<script type="text/javascript" src="assets/jQalertbox/jQalertbox.min.js"></script>
    <link href="assets/custom.css" rel="stylesheet">
    <link href="assets/main.d810cf0ae7f39f28f336.css" rel="stylesheet">
    <script>var token = '{{ csrf_token() }}'; </script>
</head>
<body>
	<div class="app-main">
		<div class="app-main__outer___">
			<div class="app-main__inner p-0">
				<div class="app-inner-layout chat-layout col-md-12">
					<div class="app-inner-layout__header text-white bg-premium-dark">
						<div class="app-page-title">
							<div class="page-title-wrapper">
								<div class="page-title-heading">
									<div class="page-title-icon">
										<img src="assets/images/comments-512.png">
									</div>
									<div>{{ trans('comment.title') }}</div>
								</div>
							</div>
						</div>
					</div>
					
						
					
					<div class="app-inner-layout__wrapper">
						<div class="app-inner-layout__content card">
							<div class="table-responsive">
								<div class="chat-wrapper">
									@foreach($comments as $list)
									<div class="chat-box-wrapper">
										<div>
											<div class="avatar-icon-wrapper mr-1">
												<div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div>
												<div class="avatar-icon avatar-icon-lg rounded"><img src="assets/images/default-user.jpg" alt=""></div>
											</div>
										</div>
										<div>
											<div class="chat-box">{{ $list->message }}</div>
											<small class="opacity-6"><i class="fa fa-user">{{ $list->name }}</i> <i class="fa fa-calendar-alt mr-1"></i>{{ date('Y-m-d h:i A', strtotime($list->created_at)) }} <a class="cm-reply" href="javascript:void(0);" onclick="CommentBox('{{ $list->id }}')"><i class="fa fa-comment"></i></a></small>
											
											<!-- Inner Comment Box-->
											@if($list->subcomment)
												@foreach($list->subcomment as $sublist)
												<div class="chat-box-wrapper">
													<div>
														<div class="avatar-icon-wrapper mr-1">
															<div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div>
															<div class="avatar-icon avatar-icon-lg rounded"><img src="assets/images/default-user.jpg" alt=""></div>
														</div>
													</div>
													<div>
														<div class="chat-box">{{ $sublist->message }}</div>
														<small class="opacity-6"><i class="fa fa-user">{{ $sublist->name }}</i> <i class="fa fa-calendar-alt mr-1"></i>{{ date('Y-m-d h:i A', strtotime($sublist->created_at)) }} <a class="cm-reply" href="javascript:void(0);" onclick="CommentBox('{{ $sublist->id }}')"><i class="fa fa-comment"></i></a></small>
														
														<!-- 3rd Comment Box-->
														@if($sublist->subcomment)
															@foreach($sublist->subcomment as $subsublist)
															<div class="chat-box-wrapper">
																<div>
																	<div class="avatar-icon-wrapper mr-1">
																		<div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div>
																		<div class="avatar-icon avatar-icon-lg rounded"><img src="assets/images/default-user.jpg" alt=""></div>
																	</div>
																</div>
																<div>
																	<div class="chat-box">{{ $sublist->message }}</div>
																	<small class="opacity-6"><i class="fa fa-calendar-alt mr-1"></i>{{ date('Y-m-d h:i A', strtotime($list->created_at)) }}</small>
																</div>
															</div>
															@endforeach
														@endif
													</div>
												</div>
												@endforeach
											@endif
										</div>
									</div>
									@endforeach
								</div>
								
								<div class="app-inner-layout__bottom-pane d-block text-center">
									<form action="javascript:void(0);" onsubmit="addComment();" class="ai-cmbox-0">
										<div class="mb-0 position-relative row form-group">
											<div class="col-sm-12">
												<div class="row">
													<div class="col-md-6 text-left">
														<input type="text" name="name" id="name" class="form-control" placeholder="{{ trans('comment.enter_name') }}"/ required>
														<div class="validation-div val-name"></div><br>
													</div>
													<div class="col-md-6 text-left">
														<input type="email" name="email" id="email" class="form-control" placeholder="{{ trans('comment.enter_email') }}"/ required>
														<div class="validation-div val-email"></div><br>
													</div>
												</div>
											</div>
											<div class="col-sm-12 text-left">
												<textarea name="text" rows="4" id="comment" class="form-control" placeholder="{{ trans('comment.write_here_to_send') }}" required></textarea>
												<div class="validation-div val-comment"></div><br>
											</div>
											<div class="col-sm-12 text-left">
												<button type="submit" class="mt-1 btn btn-primary">{{ trans('comment.submit') }}</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="app-drawer-overlay d-none animated fadeIn"></div>
    <script type="text/javascript" src="assets/scripts/main.d810cf0ae7f39f28f336.js"></script>
	<script type="text/javascript" src="assets/custom.js"></script>
	@include('custom-js');
</body>
</html>