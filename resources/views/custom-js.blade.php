<script>
  $(document).ready(function(e) {
	
	//
  });
  
  function 	CommentBox(parent_id = 0){
	  
	  jQbox({
			title: '{{ trans("comment.comment_reply") }}',
			showConfirmButton: false,
			width: '55%',
			html: '<form action="javascript:void(0);" onsubmit="addComment('+ parent_id +');" class="ai-cmbox-'+ parent_id +'"><div class="mb-0 position-relative row form-group"><div class="col-sm-12"><div class="row"><div class="col-md-6 text-left"><input type="text" name="name" id="name" class="form-control" placeholder="Enter name"/ required><div class="validation-div val-name"></div><br></div><div class="col-md-6 text-left"><input type="email" name="email" id="email" class="form-control" placeholder="Enter Email"/ required><div class="validation-div val-email"></div><br></div></div></div><div class="col-sm-12 text-left"><textarea name="text" rows="4" id="comment" class="form-control" placeholder="Write here to send..." required></textarea><div class="validation-div val-comment"></div><br></div><div class="col-sm-12"><button type="submit" class="mt-1 btn btn-primary">{{ trans("comment.submit") }}</button></div></div></form>',
	  });
  }

  // ADD COMMENT
  function 	addComment(parent_id = 0){
	 var box = '.ai-cmbox-' + parent_id;
	var data = new FormData();
	data.append('name', $(box + ' #name').val());
	data.append('email', $(box + ' #email').val());
	data.append('comment', $(box + ' #comment').val());
	data.append('parent_id', parent_id);
	var response = runAjax('{{route("addComment")}}', data);
	if(response.status == '200'){
		Swal.fire({
		  type: 'success',
		  title: response.message,
		  showConfirmButton: false,
		  timer: 1500
		})
		setTimeout(function(){
			location.reload();
		},2000)
	}else if(response.status == '201'){
		$('.validation-div').text('');
		$.each(response.error, function( index, value ) {
			$(box + ' .val-'+index).text(value);
		});
	}
  }
</script>
