<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CommonController;

use Illuminate\Http\Request;
use App\Models\Helpers\CommonHelper;
use Validator,Auth,DB;
use Illuminate\Validation\Rule;
use App\Comment;
use Illuminate\Validation\ValidationException;


class CommentController extends CommonController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		// VALIDATION
		$validator = Validator::make($request->all(), [
            'name' => 'required|min:5|max:55',
            'email' => 'required|min:5|max:55',
            'comment' => 'required|min:3|max:1000',
        ]);
        if($validator->fails()){
            $this->ajaxValidationError($validator->errors(), trans('comment.error'));
        }
		
		DB::beginTransaction();
        try {
            $data = array(
			  'name'		=> $request->name ? $request->name : '',
			  'email'		=> $request->email ? $request->email : '',
			  'parent_id'	=> $request->parent_id ? $request->parent_id : '0',
			  'message'		=> $request->comment ? $request->comment : '',
			);
			
			// INSERT RECORDS
			$return = Comment::create($data);
			if($return){
                DB::commit();
                $this->sendResponse([], trans('comment.comment_success'));
            } else {
                DB::rollback();
                $this->ajaxError([], trans('comment.comment_error'));
            }
        } catch (Exception $e) {
            DB::rollback();
            $this->ajaxError([], $e->getMessage());
        }
    }
}
