<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CommonController;

use Illuminate\Http\Request;
use App\Models\Helpers\CommonHelper;
use Validator,Auth,DB;
use Illuminate\Validation\Rule;
use App\Comment;
use Illuminate\Validation\ValidationException;


class HomeController extends CommonController
{
    
    /**
     * Display a listing of the resource.
     */

     /** @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
			// GET LIST
			$comments = Comment::where('parent_id', '=', '0')->get();
			if(!empty($comments)){
				foreach($comments as $ckey=> $clist){
					$comments[$ckey]['subcomment'] = [];
					$subcomment = Comment::where('parent_id', '=', $clist->id)->get();
					if(!empty($subcomment)){
						foreach($subcomment as $skey=> $slist){
							$subcomment[$skey]['subcomment'] = Comment::where('parent_id', '=', $slist->id)->get();
						}
						$comments[$ckey]['subcomment'] = $subcomment;
					}
				}
			}
			
			return view('home', compact('comments'));
        } catch (Exception $e) {
            $comments = [];
			return view('home', compact('comments'));
        } 
    }
}
