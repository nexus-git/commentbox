<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = ['parent_id','message','name','email'];

    /**
     * @return mixed
     */
    // public function country(){
    //     return $this->belongsTo(Country::class, 'country_id' );
    // }

    // public function city(){
    //     return $this->hasMany('App\Models\City','state_id','id' );
    // }
}
